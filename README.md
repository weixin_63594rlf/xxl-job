# 注意事项

1. docker 版的不能修改默认配置，如有需要可以自行打包 image 文件
2. other.txt 用于记录可能会用到的命令，如将 xxl-job app.jar 从容器中拷贝出来
3. xxl-job 使用数据库锁，短任务消耗大
4. 默认管理界面为：http://localhost:9090/xxl-job-admin/
5. 默认用户名：admin 默认密码：123456
6. 添加的执行器必须和 application.properties 中的 xxl.job.executor.appname 相匹配
![img.png](doc/png/img.png)
![img_1.png](doc/png/img_1.png)
7. 向调度中心发送 SUCCESS 调度中心会停止该任务的调度
![img.png](doc/png/img_2.png)
![img_3.png](doc/png/img_3.png)