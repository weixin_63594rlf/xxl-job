package org.spring.xxljob1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XxlJob1Application {

    public static void main(String[] args) {
        SpringApplication.run(XxlJob1Application.class, args);
    }

}
