package org.spring.xxljob1.app.properties.xxl.job;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties("xxl.job.admin")
public class XxlJobAdminProperties {
    private String adminAddresses;
}
