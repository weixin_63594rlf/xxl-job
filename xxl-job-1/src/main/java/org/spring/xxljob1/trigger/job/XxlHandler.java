package org.spring.xxljob1.trigger.job;

import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.stereotype.Component;

@Component
public class XxlHandler {

    @XxlJob("test")
    public void xxlJob() {
        System.out.println("xxlJob");
    }
}
