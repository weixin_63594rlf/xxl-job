package org.spring.xxljob2.app.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties("xxl.job")
public class XxlJobProperties {
    private String accessToken;


}
