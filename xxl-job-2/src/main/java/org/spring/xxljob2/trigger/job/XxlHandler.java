package org.spring.xxljob2.trigger.job;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

@Component
public class XxlHandler {
    AtomicInteger atomicInteger = new AtomicInteger(0);

    @XxlJob("handler")
    public ReturnT<String> handler() {
        if (atomicInteger.getAndIncrement() < 5) {
            System.out.println(atomicInteger.incrementAndGet());
            return ReturnT.FAIL;
        } else {
            return ReturnT.SUCCESS;
        }

    }
}
